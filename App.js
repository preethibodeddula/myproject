import React from 'react';
import { BrowserRouter, Switch, Route} from 'react-router-dom';
 import Dashboard from './Dashboard';
import Login from './Login';
import Signup from './Signup';
 import Payments from './Payments';
 import Status from './status';
import AdminLogin from './AdminLogin';
import AdminDashboard from './AdminDashboard';
import Cartpage from './cartpage';
function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <div>
         
          <div className="content">
            <Switch>
              <Route exact path="/" component={Login} />
              <Route path="/signup" component={Signup}/>
              <Route path="/adminlogin" component={AdminLogin}/>
              <Route path="/dashboard" component={Dashboard} />
              <Route path="/payments" component={Payments} />
              <Route path="/admindashboard" component={AdminDashboard} />
              <Route path="/status" component={Status} />
              <Route path="/cartpage" component={Cartpage} />
            </Switch>
          </div>
        </div>
      </BrowserRouter>
     
    </div>
  );
}
 
export default App;
